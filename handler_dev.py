"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate trip event for dev environment.
"""

from dashboard import Dashboard

def handler():
    #dashboard = Dashboard('dev', 'bruno-stream-test')
    dashboard = Dashboard('dev', 'test')
    dashboard.handler()

handler()
